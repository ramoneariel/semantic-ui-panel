 /**
 * # Semantic UI - Module
 * http://github.com/quirkyinc/semantic
 * 
 *  @autor ramoneariel@gmail.com 
 *  version 1.0
 * Copyright 2018 Contributors
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 **/

;
(function($, window, document, undefined) {

    $.fn.panel = function(parameters) {

        var

        $allModules = $(this),
            $document = $(document),

            moduleSelector = $allModules.selector || '',

            time = new Date().getTime(),
            performance = [],

            query = arguments[0],
            methodInvoked = (typeof query == 'string'),
            queryArguments = [].slice.call(arguments, 1),
            returnedValue;

        $allModules.each(function() {
            var
            settings = ($.isPlainObject(parameters)) ? $.extend(true, {}, $.fn.panel.settings, parameters) : $.extend({}, $.fn.panel.settings),

                namespace = settings.namespace,
                error = settings.error,
                className = settings.className,

                eventNamespace = '.' + namespace,
                moduleNamespace = 'module-' + namespace,

                instance = $(this).data(moduleNamespace),
                element = this,
                $module = $(this),

                $close = $module.find(settings.selector.close),

                observer,
                module;

            module = {

                initialize: function() {
                    module.debug('Initializing module for', element);
                    module.bind.events();
                    module.instantiate();
                },

                instantiate: function() {
                    module.verbose('Storing instance of module');
                    instance = module;
                    $module.data(moduleNamespace, instance);
                },

                observeChanges: function() {
                    if ('MutationObserver' in window) {
                        observer = new MutationObserver(function(mutations) {
                            module.debug('Element updated refreshing selectors');
                            module.refresh();
                        });
                        observer.observe(element, {
                            childList: true,
                            subtree: true
                        });
                        module.debug('Setting up mutation observer', observer);
                    }
                },

                destroy: function() {
                    module.verbose('Destroying previous module for', element);
                    $module.removeData(moduleNamespace)
                        .off(eventNamespace);
                },

                refresh: function() {
                    module.verbose('Refreshing elements', element);
                    $module = $(element);
                },

                bind: {
                    events: function() {
                        $module.on('click' + eventNamespace, settings.selector.close, module.event.close);
                    }
                },
                event: {
                    close: function(event) {
                        $module.transition(settings.selector.animation)
                    }
                },

                show: function(callback) {
                    callback = $.isFunction(callback) ? callback : function() {};
                    module.refresh();
                    module.showPanel(callback);
                },

                showPanel: function(callback) {
                    callback = $.isFunction(callback) ? callback : function() {};

                    $module.transition(settings.selector.animation).css('width', settings.selector.ancho);

                    $module.niceScroll({
                        cursorcolor: "#3d3b3b",
                        cursorwidth: 5,
                        cursorborderradius: 0,
                        cursorborder: 0,
                        scrollspeed: 50,
                        autohidemode: true,
                        zindex: 9999999
                    });

                },

                setting: function(name, value) {
                    module.debug('Changing setting', name, value);
                    if ($.isPlainObject(name)) {
                        $.extend(true, settings, name);
                    } else if (value !== undefined) {
                        settings[name] = value;
                    } else {
                        return settings[name];
                    }
                },

                internal: function(name, value) {
                    if ($.isPlainObject(name)) {
                        $.extend(true, module, name);
                    } else if (value !== undefined) {
                        module[name] = value;
                    } else {
                        return module[name];
                    }
                },

                debug: function() {
                    if (settings.debug) {
                        if (settings.performance) {
                            module.performance.log(arguments);
                        } else {
                            module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
                            module.debug.apply(console, arguments);
                        }
                    }
                },

                verbose: function() {
                    if (settings.verbose && settings.debug) {
                        if (settings.performance) {
                            module.performance.log(arguments);
                        } else {
                            module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
                            module.verbose.apply(console, arguments);
                        }
                    }
                },

                error: function() {
                    module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
                    module.error.apply(console, arguments);
                },

                performance: {
                    log: function(message) {
                        var
                        currentTime,
                        executionTime,
                        previousTime;
                        if (settings.performance) {
                            currentTime = new Date().getTime();
                            previousTime = time || currentTime;
                            executionTime = currentTime - previousTime;
                            time = currentTime;
                            performance.push({
                                'Name': message[0],
                                'Arguments': [].slice.call(message, 1) || '',
                                'Element': element,
                                'Execution Time': executionTime
                            });
                        }
                        clearTimeout(module.performance.timer);
                        module.performance.timer = setTimeout(module.performance.display, 500);
                    },
                    display: function() {
                        var
                        title = settings.name + ':',
                            totalTime = 0;
                        time = false;
                        clearTimeout(module.performance.timer);
                        $.each(performance, function(index, data) {
                            totalTime += data['Execution Time'];
                        });
                        title += ' ' + totalTime + 'ms';
                        if (moduleSelector) {
                            title += ' \'' + moduleSelector + '\'';
                        }
                        if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
                            console.groupCollapsed(title);
                            if (console.table) {
                                console.table(performance);
                            } else {
                                $.each(performance, function(index, data) {
                                    console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                                });
                            }
                            console.groupEnd();
                        }
                        performance = [];
                    }
                },

                invoke: function(query, passedArguments, context) {
                    var
                    object = instance,
                        maxDepth,
                        found,
                        response;
                    passedArguments = passedArguments || queryArguments;
                    context = element || context;
                    if (typeof query == 'string' && object !== undefined) {
                        query = query.split(/[\. ]/);
                        maxDepth = query.length - 1;
                        $.each(query, function(depth, value) {
                            var camelCaseValue = (depth != maxDepth) ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
                            if ($.isPlainObject(object[camelCaseValue]) && (depth != maxDepth)) {
                                object = object[camelCaseValue];
                            } else if (object[camelCaseValue] !== undefined) {
                                found = object[camelCaseValue];
                                return false;
                            } else if ($.isPlainObject(object[value]) && (depth != maxDepth)) {
                                object = object[value];
                            } else if (object[value] !== undefined) {
                                found = object[value];
                                return false;
                            } else {
                                module.error(error.method, query);
                                return false;
                            }
                        });
                    }
                    if ($.isFunction(found)) {
                        response = found.apply(context, passedArguments);
                    } else if (found !== undefined) {
                        response = found;
                    }
                    if ($.isArray(returnedValue)) {
                        returnedValue.push(response);
                    } else if (returnedValue !== undefined) {
                        returnedValue = [returnedValue, response];
                    } else if (response !== undefined) {
                        returnedValue = response;
                    }
                    return found;
                }
            };

            if (methodInvoked) {
                if (instance === undefined) {
                    module.initialize();
                }
                module.invoke(query);
            } else {
                // when re-initializing an element make sure the previous one is torn down first
                if (instance !== undefined) {
                    instance.invoke('destroy');
                }
                module.initialize();
            }
        });
        return (returnedValue !== undefined) ? returnedValue : $allModules;
    };

    $.fn.panel.settings = {

        name: 'Panel',
        debug: true,
        verbose: false,
        performance: false,
        namespace: 'panel',

        selector: {
            panel: '.panel',
            close: '> .header',
            animation: ' fly left',
            ancho: '60%'
        },

        regExp: {
            text: /\s\S*/
        },

        className: {
            disabled: 'disabled'
        },
    };

})(jQuery, window, document);