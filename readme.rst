###################
PANEL Plug-in ver 1.0
###################

Es un panel que ocupa el 60% de la pantalla. Se desliza de derecha a izquerda.

*******************
Información
*******************

El codigo esta basado en un modulo basico adaptado a mis necesidades.


*******************
Requerimientos
*******************

Requiere jquery.nicescroll.js


*******************
USO
*******************

<div class="ui panel right">
	<div class="header"></div>
	<div class="content">
	<!-- CONTENIDO DEL PANEL -->
	</div>
</div>

<script>

$(".ui.panel").panel("show");
	
